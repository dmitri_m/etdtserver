Test Echo, Time, Daytime server

Building
--------------------------------
The server is using Apache MINA NIO library.

To build the program:
1. cd etdtserver
2. Run 
	mvn package

jar will be created in the directory ./target 
Dependencies will be automatically copied to the ./target as well.

Running
--------------------------------
To run the server, execute
java -jar target\etdtserver-1.0-SNAPSHOT.jar TCP_ECHO_PORT UDP_ECH_PORT TCP_TIME_PORT UDP_TIME_PORT TCP_DAYTIME_PORT UDP_DAYTIME_PORT

for example
java -jar target\etdtserver-1.0-SNAPSHOT.jar 40000 40001 40002 40003 40004 40005

There are no default settings for the ports, program will exit if some of the ports are not specified.






