package dmitri.codingtest.etdtserver;

import org.apache.mina.core.service.IoHandler;
import org.apache.mina.core.session.IdleStatus;
import org.apache.mina.core.session.IoSession;

/**
 * Default implementation of MINA IoHandler.
 *
 */
public class Handler implements IoHandler{
	private String name;

	public Handler(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void exceptionCaught(IoSession session, Throwable cause)
			throws Exception {
		System.out.println(getName() + " exception: " + cause);
		cause.printStackTrace();
		session.close(true);
	}

	public void messageReceived(IoSession session, Object message)
			throws Exception {
		System.out.println(getName() + " messageReceived: [" + message.toString() + "]");
	}

	public void messageSent(IoSession session, Object message) throws Exception {
		System.out.println(getName() + " messageSent: [" + message + "]");
	}

	public void sessionClosed(IoSession session) throws Exception {
		System.out.println(getName() + " sessionClosed");
	}

	public void sessionCreated(IoSession session) throws Exception {
		System.out.println(getName() + " sessionCreated");
	}

	public void sessionIdle(IoSession session, IdleStatus status)
			throws Exception {
		System.out.println(getName() + " sessionIdle");
	}

	public void sessionOpened(IoSession session) throws Exception {
		System.out.println(getName() + " sessionOpened");
	}
}
