package dmitri.codingtest.etdtserver;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;

import org.apache.mina.core.service.IoAcceptor;
import org.apache.mina.filter.codec.ProtocolCodecFilter;
import org.apache.mina.filter.codec.textline.TextLineCodecFactory;

/**
 * Listener class.
 * 
 * Starts a server on specified port and assigns specified handler.
 * Can be used for listening both TCP and UDP, depending on the IoAcceptor 
 * passed to the constructor
 */
public class Listener {

	private int port;
	private Handler handler;
	private IoAcceptor acceptor;

	public Listener(int port, IoAcceptor acceptor, Handler handler) {
		this.port = port;
		this.acceptor = acceptor;
		this.handler = handler;
	}

	public void start() {
        this.acceptor.getFilterChain().addLast( "codec", new ProtocolCodecFilter( 
        		new TextLineCodecFactory( Charset.forName("UTF-8"))));
        this.acceptor.setHandler(this.handler);
        
        try {
			this.acceptor.bind( new InetSocketAddress(this.port) );
			System.out.println(this.handler.getName() + " started on port: " + this.port);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
