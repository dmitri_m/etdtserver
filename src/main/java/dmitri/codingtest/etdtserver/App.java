package dmitri.codingtest.etdtserver;

import org.apache.mina.transport.socket.nio.NioDatagramAcceptor;
import org.apache.mina.transport.socket.nio.NioSocketAcceptor;

public class App 
{
    public static void main( String[] args )
    {
    	if (args.length != 6) {
    		System.out.println("Format:");
    		System.out.println("ETDTServer TCP_ECHO_PORT UDP_ECHO_PORT TCP_TIME_PORT UDP_TIME_PORT TCP_DAYTIME_PORT UDP_DAYTIME_PORT");
    		return;
    	}
    	
    	int tcpEchoPort = Integer.parseInt(args[0]);
    	int udpEchoPort = Integer.parseInt(args[1]);
    	int tcpTimePort = Integer.parseInt(args[2]);
    	int udpTimePort = Integer.parseInt(args[3]);
    	int tcpDaytimePort = Integer.parseInt(args[4]);
    	int udpDaytimePort = Integer.parseInt(args[5]);

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
				System.out.println("Application terminated");
            }
        });

        new Listener(tcpEchoPort, new NioSocketAcceptor(), new EchoHandler("[TCP Echo]")).start();
    	new Listener(tcpTimePort,  new NioSocketAcceptor(), new TimeHandler("[TCP Time]")).start();
    	new Listener(tcpDaytimePort,  new NioSocketAcceptor(), new DaytimeHandler("[TCP DayTime]")).start();
    
    	new Listener(udpEchoPort, new NioDatagramAcceptor(), new EchoHandler("[UDP Echo]")).start();
    	new Listener(udpTimePort, new NioDatagramAcceptor(), new TimeHandler("[UDP Time]")).start();
    	new Listener(udpDaytimePort, new NioDatagramAcceptor(), new DaytimeHandler("[UDP DayTime]")).start();
    }
}
