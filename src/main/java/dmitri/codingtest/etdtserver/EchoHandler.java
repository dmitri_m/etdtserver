package dmitri.codingtest.etdtserver;

import org.apache.mina.core.session.IoSession;

/**
 * ECHO protocol handler
 */
public class EchoHandler extends Handler {

	public EchoHandler(String name) {
		super(name);
	}
	
	@Override
	public void messageReceived(IoSession session, Object message)
			throws Exception {
		super.messageReceived(session, message);
		session.write(message);
	}
}
