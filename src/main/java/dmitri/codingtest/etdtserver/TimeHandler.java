package dmitri.codingtest.etdtserver;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import org.apache.mina.core.session.IoSession;

/**
 * 
 * TIME protocol handler
 *
 */
public class TimeHandler extends Handler {

	private static final TimeZone TIME_ZONE_GMT = TimeZone.getTimeZone("GMT");
	private static final long TIME_ORIGIN = calculateOrigin();
	
	private static long calculateOrigin() {
		Calendar c = Calendar.getInstance(TIME_ZONE_GMT);
		c.set(1900, 1, 1, 0, 0, 0);
		return c.getTimeInMillis()/1000;
	}

	public TimeHandler(String name) {
		super(name);
	}

	@Override
	public void sessionOpened(IoSession session) throws Exception {
		super.sessionOpened(session);
		
		long now = new Date().getTime() / 1000;
		session.write(now - TIME_ORIGIN);
		session.close(false);
	}
}
