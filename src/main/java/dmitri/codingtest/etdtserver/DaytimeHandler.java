package dmitri.codingtest.etdtserver;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.mina.core.session.IoSession;

/**
 * 
 * DAYTIME protocol handler
 *
 */
public class DaytimeHandler extends Handler {

	public DaytimeHandler(String name) {
		super(name);
	}
	
	@Override
	public void sessionOpened(IoSession session) throws Exception {
		super.sessionOpened(session);
		
		session.write(new SimpleDateFormat().format(new Date()));
		session.close(false);
	}

}
